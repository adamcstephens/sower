module codeberg.org/adamcstephens/sower

go 1.23.6

require (
	github.com/adrg/xdg v0.5.3
	github.com/alexflint/go-arg v1.5.1
	github.com/golang-queue/queue v0.3.0
	github.com/lmittmann/tint v1.0.7
	github.com/mattn/go-isatty v0.0.20
	github.com/nshafer/phx v0.2.3
	github.com/oapi-codegen/oapi-codegen/v2 v2.4.1
	github.com/oapi-codegen/runtime v1.1.1
)

require (
	github.com/alexflint/go-scalar v1.2.0 // indirect
	github.com/apapsch/go-jsonmerge/v2 v2.0.0 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/gorilla/websocket v1.5.3 // indirect
	github.com/jpillora/backoff v1.0.0 // indirect
	golang.org/x/sys v0.30.0 // indirect
)
