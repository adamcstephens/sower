defmodule Sower.Repo do
  use Ecto.Repo,
    otp_app: :sower,
    adapter: Ecto.Adapters.Postgres

  require Ecto.Query

  @tenant_key {__MODULE__, :org_id}

  @impl true
  def init(_context, config) do
    {:ok, Keyword.merge(config, Application.get_env(:sower, :database, []))}
  end

  @doc """
  Enable foreign key multitenancy and require :org_id unless :skip_org_id is passed
  """
  @impl true
  def prepare_query(_operation, query, opts) do
    cond do
      opts[:skip_org_id] || opts[:ecto_query] in [:schema_migration, :preload] ||
          opts[:schema_migration] ->
        {query, opts}

      org_id = opts[:org_id] ->
        {Ecto.Query.where(query, org_id: ^org_id), opts}

      true ->
        raise "expected org_id or skip_org_id to be set"
    end
  end

  def put_org_id(org_id) do
    Process.put(@tenant_key, org_id)
  end

  def get_org_id() do
    Process.get(@tenant_key)
  end

  @doc """
  Read the org id by default on operations
  """
  @impl true
  def default_options(_operation) do
    [org_id: get_org_id()]
  end
end
