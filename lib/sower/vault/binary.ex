defmodule Sower.Vault.Binary do
  use Cloak.Ecto.Binary, vault: Sower.Vault
end
