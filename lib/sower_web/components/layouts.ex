defmodule SowerWeb.Layouts do
  use SowerWeb, :html

  embed_templates "layouts/*"
end
