defmodule SowerWeb.AuthHTML do
  use SowerWeb, :html

  embed_templates "auth_html/*"
end
