# This file is responsible for configuring your application
# and its dependencies with the aid of the Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

config :sower, ecto_repos: [Sower.Repo]
config :sower, Sower.Repo, migration_primary_key: [type: :identity]

# Configures the endpoint
config :sower, SowerWeb.Endpoint,
  adapter: Bandit.PhoenixAdapter,
  render_errors: [
    formats: [html: SowerWeb.ErrorHTML, json: SowerWeb.Api.ErrorJSON],
    layout: false
  ],
  pubsub_server: Sower.PubSub,
  live_view: [signing_salt: "nrwHFIM7"]

# Configure esbuild (the version is required)
config :esbuild,
  version: "0.17.11",
  default: [
    args:
      ~w(js/app.js --bundle --target=es2017 --outdir=../priv/static/assets --external:/fonts/* --external:/images/*),
    cd: Path.expand("../assets", __DIR__),
    env: %{"NODE_PATH" => Path.expand("../deps", __DIR__)}
  ]

# Configure tailwind (the version is required)
config :tailwind,
  version: "3.3.2",
  default: [
    args: ~w(
      --config=tailwind.config.js
      --input=css/app.css
      --output=../priv/static/assets/app.css
    ),
    cd: Path.expand("../assets", __DIR__)
  ]

# Configures Elixir's Logger
config :logger, :console,
  level: :debug,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# config :opentelemetry, :resource, service: %{name: "sower"}
#
# config :opentelemetry,
#   span_processor: :batch,
#   traces_exporter: :otlp
#
# config :opentelemetry_exporter,
#   otlp_protocol: :http_protobuf,
#   otlp_endpoint: "http://127.0.0.1:4318"

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{config_env()}.exs"
